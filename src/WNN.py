# -*- coding: utf-8 -*-
"""
Created on Fri May  8 20:07:52 2020

@author: King
"""

#package importálás
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
#adatsor beolvasása
###############################
#X (bemenet) és Y(kimenet) értékek, adatsortól függ hogyan
data = loadmat("Lab7data.mat")   #ha .mat fájlból töltjük fel, ez a gyakorlat anyaga
X = data["X"] # a fájlból kiszedjük a bemeneteket
X = X[:,0:25]
y = np.ones((5000), dtype=int) # és a cimkéket
del data # majd töröljük
###############################
#aktivációs függvények
###############################
def sigmoid(z):
    g = 1/(1+np.exp(-z))    
    return g
def sigmoidGradient(z):  
    g_deriv=sigmoid(z)*(np.ones(np.shape(z))-sigmoid(z))
    return g_deriv
def gauss(z):
    g = -z*np.exp(-0.5*np.square(z))
    return g
def gaussGradient(z):
    g_deriv=(z-np.ones(np.shape(z)))*np.exp(-0.5*np.square(z))
    return g_deriv
def MexicanHat(z):
    g = (np.ones(np.shape(z))-np.square(z))*np.exp(-0.5*np.square(z))
    return g
def MexicanHatGradient(z):
    g_deriv = -np.exp(-0.5*np.square(z))-2*z*np.exp(-0.5*np.square(z))+np.square(z)*np.exp(-0.5*np.square(z))
    return g_deriv
def MorletWavelet(z):
    g = np.cos(5*z)*np.exp(-0.5*np.square(z))
    return g
def MorletWaveletGradient(z):
    g_deriv = -5*np.sin(5*z)*np.exp(-0.5*np.square(z))-np.cos(5*z)*np.exp(-0.5*np.square(z))
    return g_deriv    
###############################
# szükséges mátrixok: 
#X bemeneti    n*(i+1) (+1 -> bias)
#Y kimeneti    n*h 
#wk1_ij  - bemenet-rejtett neuronok transzlációs mátrixa (i+1)*j (j db rejtett neuron)
#we1_ij  - bemenet-rejtett neuronok dilatációs mátrixa   (i+1)*j (j db rejtett neuron)
#w2   -  rejtett neuronok-kimenet súlyai (j+1)*h (+1->bias tag)
#w0   -  bemenet-kimenet közvetlen súlyai (i+1)*h
#Phi    - wavelet function mátrixa wk1_ij, we1_ij és X felhasználásával (i+1)*j
#X = np.ones((200, 12))*2 # 200 minta, mintánként 12 bemenet biasssal kiegészítve
#y = np.ones((200, 1), dtype=int) # 200 mint, 5 lehetséges osztály 

j = 25 #10db rejtett neuron
WK1_ij = np.ones((np.shape(X)[1], j))*-1
WE1_ij = np.ones((np.shape(X)[1], j)) 
W2 = np.ones((j+1, 10)) #példa-> 5db osztály
W0 = np.ones((np.shape(X)[1], 10))

#zi mátrixok
####################


def Phi(WK1_ij, WE1_ij, X): # HN-hidden neurons = np.shape(wk1_ij[1]) csak a követhetőségért
    m = np.shape(X)[0]
    n = np.shape(X)[1]
    HN = np.shape(WK1_ij)[1]
    Z = []
    FI = np.ones((m, HN))
    FId = np.ones((m, HN))
    for i in range(n):
        seged = np.zeros((m, HN))
        for k in range(m):
            for l in range(HN):
                seged[k, l] = (X[k, l]-WK1_ij[i, l])/WE1_ij[i, l]
        Z.append(seged)
        FI = FI*gauss(seged)
    FId=np.array(gaussGradient(Z))
    Z = np.array(Z)
    
    return Z, FI, FId

####################
#cost, grad function
def CostGrad(X,y,wk1_ij,we1_ij,w2, w0, class_num, Lambda):
    m = np.shape(X)[0]
    n = np.shape(wk1_ij)[0]
    l = np.shape(wk1_ij)[1]
    Z, FI, FID =Phi(wk1_ij, we1_ij, X)
    print(FI.shape)
    #print(wk1_ij.shape)
    #print(we1_ij.shape)
    #print(w2.shape)
    #print(w0.shape)
   #Forward step
   #####################   
    FI_bias = np.column_stack((np.ones(m), FI))
    Y1 = FI_bias.dot(w2)
    #Y2 = X.dot(w0)
    #print(Y1.shape)
    #print(Y2.shape)
    Ypred = sigmoid(Y1)
    #print(y.shape)
    ####################
    #ONE HOT ENCODING
    ####################
    Y=np.zeros((len(y),class_num))
    print(Y.shape)
    #pred_formed=np.zeros((len(pred),num_labels))
    for i in range(y.shape[0]):
        value=y[i]-1 # ha 1-től kezdve nevezzük el a kimeneteket
        Y[i,value] = 1
    ####################
    #Penalties for the weights (for COST function)
    ####################
    pen_wk1_ij = (np.sum((wk1_ij)**2)-np.sum(wk1_ij[:,0]**2))*Lambda/2/m #BIAS tagokkal nem büntetjük
    pen_we1_ij = (np.sum((we1_ij)**2)-np.sum(wk1_ij[:,0]**2))*Lambda/2/m #BIAS tagokkal nem büntetjük
    pen_w2 = (np.sum((w2)**2)-np.sum(w2[:,0]**2))*Lambda/2/m #BIAS tagokkal nem büntetjük
    #pen_w0 = (np.sum((w0)**2)-np.sum(w0[:,0]**2))*Lambda/2/m #BIAS tagokkal nem büntetjük
    ####################
    #Punishment for learning
    ####################
    #BIAS tagok a 0. sorban, azokat kinullázzuk, hogy ne büntessük
    pun_wk1 = (Lambda/m)*np.vstack((np.zeros((1,wk1_ij.shape[1])),wk1_ij[1:,:]))
    pun_we1 = (Lambda/m)*np.vstack((np.zeros((1,we1_ij.shape[1])),we1_ij[1:,:]))
    pun_w2 = (Lambda/m)*np.vstack((np.zeros((1,w2.shape[1])),w2[1:,:]))
    #pun_w0 = (Lambda/m)*np.vstack((np.zeros((1,w0.shape[1])),w0[1:,:]))
    
    ####################
    #COST function ????????
    ####################
    first_member=Y*np.log(Ypred) #kiválogatjuk  amegfelleő értékeket az aktivált kimenetekből
    second_member=np.subtract(np.ones(np.shape(Y)),Y)*np.log(np.subtract(np.ones(np.shape(Y)),Ypred))
    C=np.sum(first_member+second_member,axis=None)/(-m)+pen_wk1_ij+pen_we1_ij+pen_w2
    ####################
    #Gradients
    ####################
    grad_wk1 = np.zeros(np.shape(wk1_ij))
    diff = Y-Ypred
    SG =sigmoidGradient(Y1)
    d3 = -diff*SG/m
    w2_0 = w2[1:,:]
    #grad_w0 = (-1)/m*(X.T.dot(diff)) + pun_w0
    grad_w2 = (FI_bias.T.dot(d3)) #+ pun_w2
    grad_wk1 = np.zeros(np.shape(wk1_ij)) +pun_wk1
    grad_we1 = np.zeros(np.shape(we1_ij)) + pun_we1
    in_wk1 = np.ones(np.shape(wk1_ij))/wk1_ij
    in_we1 = np.ones(np.shape(we1_ij))/we1_ij
    for i in range(n):
            grad_wk1[i, :] = (((in_we1.dot(w2_0)).dot(d3.T)).dot(FI/gauss(Z[i])*FID[i]))[i,:]/m
            grad_we1[i, :] = (((in_we1.dot(w2_0)).dot(d3.T)).dot(FI/gauss(Z[i])*FID[i]*Z[i]))[i,:]/m
    print(grad_wk1[1,:])

    
    
    ####################
    return FI_bias, Y, Y1, pun_wk1, C
FI_bias, Y, Y1, pun_wk1, C = CostGrad(X,y,WK1_ij,WE1_ij, W2, W0, 10, 2)
   