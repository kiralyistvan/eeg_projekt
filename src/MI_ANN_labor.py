# -*- coding: utf-8 -*-
"""
Created on Fri May  8 10:31:53 2020

@author: King
"""
#importálandó csomagok
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
#Adatok olvasása
data = loadmat("Lab7data.mat")   #ha .mat fájlból töltjük fel, ez a gyakorlat anyaga
X = data["X"] # a fájlból kiszedjük a bemeneteket
y = data["y"] # és a cimkéket
del data # majd töröljük
m=X.shape[0]
#kezdeti súlyok??
#mekkora legyen a rejtett réteg, mennyi az osztály??
#Tesztnek: rejtett neuronok 30db  10 db osztály??, de ez most a labor szerint
data = loadmat("Lab7Weights.mat")                   # súlyok beolvasása
w1 = np.array(data["Theta1"])
w2 = np.array(data["Theta2"])                       # súlyok elrendezése
del data
print(w1.shape)
print(w2.shape)
# w1 súlyban benne van a bias tag, esetünkben ki kell bővíteni -> pl. w1 = np.column_stack(np.ones(w1.shape[0]), w1)
# w2 súlyban benne van a bias tag, esetünkben ki kell bővíteni -> pl. w2 = np.column_stack(np.ones(w2.shape[0]), w2)

Lambda = 1                                              # Büntetés mértéke
input_layer_size = 400                                  # bemeneti réteg nagysága, ezt változtatjuk az adatsor alapján
hidden_layer_size = 25                                  # rejtett réteg nagysága,ezt változtatjuk az adatsor alapján
num_labels = 10                                         # labelek száma, ezt változtatjuk az adatsor alapján
nn_params = np.append(w1.flatten(), w2.flatten())       # kialapítjuk a w1 és w2-t és egymás után füzzük
# Aktivációs függvények #####################
def sigmoid(z):
    g = 1/(1+np.exp(-z))    
    return g
def sigmoidGradient(z):  
    g_deriv=sigmoid(z)*(np.ones(np.shape(z))-sigmoid(z))
    return g_deriv
def gauss(z):
    g = -z*np.exp(-0.5*np.square(z))
    return g
def gaussGradient(z):
    g_deriv=-np.exp(-0.5*np.square(z))+z*np.exp(-0.5*np.square(z))
    return g_deriv
def MexicanHat(z):
    g = (np.ones(np.shape(z))-np.square(z))*np.exp(-0.5*np.square(z))
    return g
def MexicanHatGradient(z):
    g_deriv = -np.exp(-0.5*np.square(z))-2*z*np.exp(-0.5*np.square(z))+np.square(z)*np.exp(-0.5*np.square(z))
    return g_deriv
def MorletWavelet(z):
    g = np.cos(5*z)*np.exp(-0.5*np.square(z))
    return g
def MorletWaveletGradient(z):
    g_deriv = -5*np.sin(5*z)*np.exp(-0.5*np.square(z))-np.cos(5*z)*np.exp(-0.5*np.square(z))
    return g_deriv    
#############################################
def Cost_grad(nn_params, input_layer_size, hidden_layer_size, num_labels, X, y, Lambda):
    w1 = nn_params[:((input_layer_size + 1) * hidden_layer_size)].reshape(hidden_layer_size, input_layer_size + 1)
    w2 = nn_params[((input_layer_size + 1) * hidden_layer_size):].reshape(num_labels, hidden_layer_size + 1)
    m = X.shape[0]
    
    C = 0
    
    w1_grad=np.zeros(np.shape(w1))
    w2_grad=np.zeros(np.shape(w2))
    
    # Kimenet számítása
    #########################################################################################
    X1_bias=np.column_stack((np.ones(m), X)) #bemenetek bővítés
    S2 = X1_bias.dot(w1.T) #rejtett réteg bemenetei
    A2 = sigmoid(S2) #rejtett neuronok aktivációja
    n = A2.shape[0] 
    X2_bias=np.column_stack((np.ones(n), A2)) # rejtett réteg kimenetienk bővítése bias taggal
    S3 = X2_bias.dot(w2.T) #kimenetek
    Ya=sigmoid(S3) #aktivált kimenetek
    #########################################################################################
    
    # ONE HOT ENCODING (y to Y),  labor alapján
    #########################################################################################
    Y=np.zeros((len(y),num_labels))
    #pred_formed=np.zeros((len(pred),num_labels))
    for elem in range(len(y)):
        item=y[elem,0]-1
        Y[elem,item]=1
    #########################################################################################

    #Büntetés
    #########################################################################################
    pen_w1 = (np.sum((w1)**2)-np.sum(w1[:,0]**2))*Lambda/2/m #Költség függvényhez
    pen_w2 = (np.sum((w2)**2)-np.sum(w2[:,0]**2))*Lambda/2/m #Költség függvényhez
    punisher_w1 = (Lambda/m)*np.hstack((np.zeros((w1.shape[0],1)),w1[:,1:])) #bias tagokat nem büntetjük
    punisher_w2 = (Lambda/m)*np.hstack((np.zeros((w2.shape[0],1)),w2[:,1:])) #bias tagokat nem büntetjük
    #########################################################################################
    
    # Cost Function
    #########################################################################################
    first_member=Y*np.log(Ya) #kiválogatjuk  amegfelleő értékeket az aktivált kimenetekből
    second_member=np.subtract(np.ones(np.shape(Y)),Y)*np.log(np.subtract(np.ones(np.shape(Y)),Ya))
    C=np.sum(first_member*second_member,axis=None)/(-m)+pen_w1+pen_w2
    #########################################################################################

    # Back Propagation
    #########################
    #w1 súlyok gradiense
    S3d = sigmoidGradient(S3)
    d3 = -(Y-Ya)/m*(S3d)
    w2_grad=(d3.T.dot(X2_bias))+punisher_w2

    #w2 súlyok gradiense
    ww = w2[:,1:26] #w1-nél nme szerepel bias tag a rejtett réteg neuronjai között
    S2d = sigmoidGradient(S2)
    d2 = d3.dot(ww)*(S2d)
    w1_grad = (d2.T.dot(X1_bias))+punisher_w1
    grad = np.append(w1_grad.flatten(), w2_grad.flatten())
    #########################
    return C, grad
#Gyakorlat anyagábvan a költségfüggvény ellenőrzése
##########################
Lambda = 0    
C, grad = Cost_grad(nn_params,input_layer_size,hidden_layer_size,num_labels,X,y,Lambda)
print("Cost at debugging parameters: %.6f \nFor Lambda = 0 it should be:  0.287629" % C)
Lambda = 3    
C, grad = Cost_grad(nn_params,input_layer_size,hidden_layer_size,num_labels,X,y,Lambda)
print("Cost at debugging parameters: %.6f \nFor Lambda = 3 it should be:  0.576051" % C)
##########################
def randInitializeWeights(L_in, L_out):
    epsilon_init = 0.12
    W = np.random.rand(L_out,L_in+1)*(2*epsilon_init)-epsilon_init
    return W
#Kezdeti súlyok inicializálása
########################## 
initial_w1 = randInitializeWeights(input_layer_size,hidden_layer_size)
initial_w2 = randInitializeWeights(hidden_layer_size,num_labels)
initial_nn_params = np.append(initial_w1.flatten(),initial_w2.flatten())
##########################

def gradientDescentnn(X, y, initial_nn_params, lr_rate, num_iters, Lambda, input_layer_size, hidden_layer_size,
                      num_labels):

    w1 = initial_nn_params[:((input_layer_size + 1) * hidden_layer_size)].reshape(hidden_layer_size, input_layer_size + 1)
    w2 = initial_nn_params[((input_layer_size + 1) * hidden_layer_size):].reshape(num_labels, hidden_layer_size + 1)
    m = len(y)
    C_history = []
    
    for i in range(num_iters):
        if (i%20==0):
            print('Iteration:', i+1)
        elif (i==num_iters-1):
            print('Done!')
        #print("Most", w2[0, 0:20])
        nn_params = np.append(w1.flatten(), w2.flatten())
        #print("Hali", nn_params, nn_params.shape)
        C, grad = Cost_grad(nn_params, input_layer_size, hidden_layer_size, num_labels, X, y, Lambda)
        w1_grad = grad[:((input_layer_size + 1) * hidden_layer_size)].reshape(hidden_layer_size,input_layer_size+1)
        w2_grad = grad[((input_layer_size + 1)* hidden_layer_size ):].reshape(num_labels,hidden_layer_size+1)
        
        w1 = w1 - (lr_rate * w1_grad) + Lambda/m*w1
        w2 = w2 - (lr_rate * w2_grad) + Lambda/m*w2
        nn_params = np.append(w1.flatten(), w2.flatten())
        
        
    
        C_history.append(C)
        
    nn_paramsFinal = np.append(w1.flatten(), w2.flatten())
    return nn_paramsFinal, C_history

#teszt
lr_rate = 0.9
num_iter = 200
Lambda = 2

nnw, nnC_history = gradientDescentnn(X,y,initial_nn_params,lr_rate,num_iter,Lambda,input_layer_size,hidden_layer_size,num_labels)
w1 = nnw[:((input_layer_size+1) * hidden_layer_size)].reshape(hidden_layer_size,input_layer_size+1)
w2 = nnw[((input_layer_size +1)* hidden_layer_size ):].reshape(num_labels,hidden_layer_size+1)

#Előrejelzés
def predict(w1,w2,X):
    m = X.shape[0]
    X1_bias=np.column_stack((np.ones(m), X))
    
    S2=X1_bias.dot((w1.T))
    A2 = sigmoid(S2)
    X2_bias=np.column_stack((np.ones(np.shape(A2)[0]), A2))
    
    S3 = X2_bias.dot((w2.T))
    Ya=sigmoid(S3)
    p = np.argmax(Ya, axis=1) + 1
    return p

def accuracy(pred,y):
    acc = (np.mean(pred[:,np.newaxis]==y))*100
    return acc
pred = predict(w1,w2,X)
pred = predict(w1,w2,X)
acc = accuracy(pred,y)

print('Training Set Accuracy after %.0f iteration (currently trained weights): %.2f %%' % (num_iter,acc))

